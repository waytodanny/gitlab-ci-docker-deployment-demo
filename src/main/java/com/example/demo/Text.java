package com.example.demo;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@NoArgsConstructor
public class Text {

    @Id
    @GeneratedValue
    private Integer id;

    private String value;

    public Text(String value) {
        this.value = value;
    }
}
