package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class DemoApplication {

  @Autowired
  private Repo repo;

  public static void main(String[] args) {
    SpringApplication.run(DemoApplication.class, args);
  }

  @GetMapping
  public String hello() {
    return repo.findAll().get(0).getValue();
  }

  @Bean
  public CommandLineRunner r(Repo repo) {
    return a -> {
      repo.deleteAll();
      repo.save(new Text("Hello slappers-continuous-deployers 6"));
    };
  }
}
