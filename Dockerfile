FROM openjdk:8-jdk-alpine

EXPOSE 8080

WORKDIR /usr/local/bin/

COPY /target/demo-0.0.1-SNAPSHOT.jar webapp.jar

CMD ["java", "-jar", "-Dspring.profiles.active=prod", "webapp.jar"]
